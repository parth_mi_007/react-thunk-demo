import { createStore,combineReducers,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import { userReducer } from '../reducers/userReducer'

const reducers = combineReducers({
    user:userReducer
})
const intialState = {}

export const store = createStore(reducers,intialState,applyMiddleware(thunk))

