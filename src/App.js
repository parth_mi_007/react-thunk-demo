import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { userAction } from "./Redux/actions/userActions";

function App() {
  const dispatch = useDispatch();
  const {users,loading,error} = useSelector((state) => state.user)

  useEffect(() => {
    dispatch(userAction());
  }, [])
  
  return (
    <div className="App">
      <h1>Redux Thunk Demo</h1>
      {loading ? (
        <h2>Loading ....</h2>
      ) : error ? (
        <h2>{error}</h2>
      ) : 
      <div className="card-main">
        {users.map((item)=>{
            return (
            <div className="card">
              <p>name :{item.name}</p>
              <p>username :{item.username}</p>
              <p>website :{item.website}</p>
              <p>email :{item.email}</p>
            </div>
            )
          })}
        </div>
      }
    </div>
  );
}

export default App;
